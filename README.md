# arontier/ad3/documents/akscore-examples

This is a repository for ak-scores app example files.

## Prerequisites

You should have 

* `<api-key-string>`: API key of your AD3 REST API client machine
* `<token-string>`: a user's auth token

to consume AD3 REST API endpoints. And
you should do the following demonstration on your AD3 REST API client machine.

## How to use

### Getting examples

First git-clone this repository and change directory into it:

```bash
git clone https://gitlab.com/arontier/coworkers/cimplrx/akscore-examples
cd akscore-examples/examples
```

### Submitting work
To submit a work, run:

```bash
curl --location --request POST 'https://rest.ad3.io/api/v1/ak-scores/works/' \
  --header 'X-API-KEY: <api-key-string>' \
  --header 'Authorization: Bearer <token-string>' \
  --form 'description=testing-ak-scores' \
  --form 'receptor=@input/MAP3K5.MULTIMODEL.protein.pdbqt' \
  --form 'ligand=@input/MAP3K5.MULTIMODEL.ligand.pdbqt'
```

If our AD3 REST API server is not heavily loaded, it will finish this example in about 10 minutes.

### Listing works

To list all submitted works, run:

```bash
curl --location --request GET 'https://rest.ad3.io/api/v1/ak-scores/works/' \
  --header 'X-API-KEY: <api-key-string>' \
  --header 'Authorization: Bearer <token-string>'
```

### Getting work back

To see specific work, run:

```bash
curl --location --request GET 'https://rest.ad3.io/api/v1/ak-scores/works/<work-id>/' \
  --header 'X-API-KEY: <api-key-string>' \
  --header 'Authorization: Bearer <token-string>'
```

where

* `<work-id>` is the value of `id` returned in the response of "listing works" API request.

A download link `outputs_link` for your submitted work is in the response of the above request.
Download the result using this link and compare with the one in `output` directory.

## References

* REST API Documentation: [https://docs.ad3.io/#/ak-scores](https://docs.ad3.io/#/ak-scores)

You may need a read access privilege to see the following references:

* Source repository: [https://github.com/arontier/akscore](https://github.com/arontier/akscore)
* Docker, workflow repository: [https://gitlab.com/arontier/ad3/akscore](https://gitlab.com/arontier/ad3/akscore)
